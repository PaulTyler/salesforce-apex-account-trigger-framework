Account Trigger Framework.
==========================

Handles all trigger types on the Account Object.  All you have to do is fill in your custom code.

Included.
=========

AccountTrigger.trigger.  

This handles all trigger types and calls the relevant method in AccountTriggerHandler.cls

AccountTriggerHandler.cls.  


Provides methods to handle the triggers.  It is here you can put your custom Apex either in the method itself or by calling methods in your own classes.  You will have to write your own unit tests.  I have provided ways to handle lists and maps as you can see by the constructor methods.

TestAcountTrigger.cls

Unit test methods.

GenericObjectMethods.

Two static methods, createUser, creates a user with a given profile so that you can create tests to be run as a specific user using the system.runas method.

The other, getAccountDetails, I quite like thsi one.  Salesforce doesn’t have the select * option so this method simulates it.  The thing I like is that it return custom fields so any further development on  the Account module that adds new custom fields, this method doesn’t need changing.

Further Development
===================

1.  Better way of handling delete and undelete triggers.  I be conducting more R&D on this.

Installation
============

If you are using an IDE like eclipse with the force.com plugin, then copy the class and its associated xml files to your project directory/src/classes.  Copy the trigger and its xml files to the /src/classes.

Save it to your sandbox and run the unit test.

Or you can go to apex classes or apex triggers on your sandbox and paste the code in.