/*  
   Created By   :P.W.Tyler   
   Created Date :20/02/2017
   Class Function: static methods for populating standard and custom salesforce objects
                   You are free to use this and unit tests or adapt this framework, just keep the author 
                   name and created date in.
                                              
   Change History: Initial - 20/02/2017

*/

public class genericObjectMethods {
	
	// create a user with a specific profile
	public static User createUser(String profileName){
		system.debug ('genericObjectMethods:createUser:Start');
		
		Profile profile = [select id, 
     	                   name from Profile 
     	                   where name =:profileName];
     	                   
        User user = new User(alias = 'TUser', 
            email='tuser@testorg.com', 
            emailencodingkey='UTF-8', 
            lastname='Testing', 
            languagelocalekey='en_US', 
            localesidkey='en_IE_EURO', // change this for your specific needs
            profileid = profile.id, 
            timezonesidkey='Europe/London', // change this for your specific needs
            username='dduser@testorg.com');
     	
     	system.debug ('genericObjectMethods:createUser:End');
     	return user;
	}   
	
	public static Account getAccountDetails(String accountName){
		//salesforce does not support SELECT * so this will return all fields and
		//any custom fields
		system.debug ('genericObjectMethods:getAccountDetails:Start');
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Account.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        
        String theQuery = 'SELECT ';
        
		for(Schema.SObjectField s : fldObjMapValues)
		{
		   String theLabel = s.getDescribe().getLabel(); 
		   String theName = s.getDescribe().getName();
		  
		   // Continue building your dynamic query string
		   theQuery += theName + ',';
		}
		
		// Trim last comma
		theQuery = theQuery.subString(0, theQuery.length() - 1);
		
		// Finalize query string
		theQuery += ' FROM Account WHERE Name = :accountName';
		system.debug ('genericObjectMethods:getAccountDetails:theQuery=' + theQuery);
		
		Account account;
		try{
		    // Make your dynamic call
		    account = Database.query(theQuery);
		} catch (Exception e){
			system.debug ('genericObjectMethods:getAccountDetails:Record not found');
		}
 
		system.debug ('genericObjectMethods:getValidAccount:End');
		
		return account;
	}
}