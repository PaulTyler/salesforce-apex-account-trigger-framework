/*  
   Created By   :P.W.Tyler   
   Created Date :20/02/2017
   Class Function: unit tests for accountTrigger
                   You are free to use this and unit tests or adapt this framework, just keep the author 
                   name and created date in.
                   
                   You don't really need the testBeforeAfterInsert and testBeforeAfterDelete test methods
                   as the code set up in testBeforeAfterUnDelete tests for insert and delete.  I have left
                   them in there just in case you don't use the testBeforeAfterUnDelete test method
                                              
   Change History: Initial - 20/02/2017

*/
@isTest
private class testAcountTrigger {
     static testMethod void testBeforeAfterInsert() {
     	//tests the before and after insert trigger code
     	//Run the test as a specific user with a specific profile
     	
     	// change the parameter to be any valid profile for testing as a specific user
     	User user = genericObjectMethods.createUser('System Administrator');
     	
     	system.runas(user){
     	                   
     	    List<Account> accts = new List<Account>();
     	
     	    // create 200 accounts - bulk test
     	    for(Integer i=0;i<200;i++) {
     	        Account a = new Account(Name='TestAccount' + i);
     	        accts.add(a);
     	    }
            insert accts;
            
            //get the account details
            Account account = genericObjectMethods.getAccountDetails('TestAccount0');
                         
            System.assertEquals(account.Name,'TestAccount0');
     	}
    }
     
    static testMethod void testBeforeAfterUpdate() {
    	
    	User user = genericObjectMethods.createUser('System Administrator');
     	
     	system.runas(user){
	    	Account a = new Account(Name='TestAccount');
	    	insert a;
	    
	    	a.name = 'Test';
	    	update a;
	    	
	    	Account account = genericObjectMethods.getAccountDetails('Test');
	    	System.assertEquals(account.Name,'Test');
     	}
    }
    
    static testMethod void testBeforeAfterDelete() {
    	
    	User user = genericObjectMethods.createUser('System Administrator');
     	
     	system.runas(user){
	    	Account a = new Account(Name='TestAccount');
	    	insert a;
	    	
	    	Account delAccount = genericObjectMethods.getAccountDetails('TestAccount');
	    	delete delAccount;
	    	
	    	Account account = genericObjectMethods.getAccountDetails('TestAccount');
     	}
    }
    
    static testMethod void testAfterUnDelete() {
    	
    	User user = genericObjectMethods.createUser('System Administrator');
     	
     	system.runas(user){
	    	Account a = new Account(Name='TestAccount');
	    	insert a;
	    	System.assertEquals(a.Name,'TestAccount');
	    	
	    	Account delAccount = genericObjectMethods.getAccountDetails('TestAccount');
	    	delete delAccount;
	    	unDelete delAccount;
	    	
	    	Account account2 = genericObjectMethods.getAccountDetails('TestAccount');
	    	System.assertEquals(account2.Name,'TestAccount');
     	}
    }
}