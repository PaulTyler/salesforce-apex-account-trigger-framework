/*  
   Created By   :P.W.Tyler   
   Created Date :20/02/2017
   Class Function: This is where you would put your business logic for each method called
                   from the Trigger handler.
                   Use is made of overloaded constructors as insert and update are handled 
                   differently with data types
                   
                   Just comment out in accountTrigger the methods that you don't need
                                              
   Change History: Initial - 20/02/2017

*/
public with sharing class AccountTriggerHandler {
	// These variables store Trigger.oldMap and Trigger.newMap
    Map<Id, Account> oldAccs;
    Map<Id, Account> newAccs;

    List<Account> oldAccounts = new List<Account>();
    List<Account> newAccounts = new List<Account>();

    // This is the constructor
    // A list of the old and new records is expected as inputs
    public AccountTriggerHandler() {
    }
    
    // This is the constructor
    // A list of the old and new records is expected as inputs
    public AccountTriggerHandler(List <Account> oldTriggerAccounts,
                                 List <Account> newTriggerAccounts) {
        
        oldAccounts = oldTriggerAccounts;
        newAccounts = newTriggerAccounts;
        
    }
    // This is the constructor
    // A map of the old and new records is expected as inputs
    public AccountTriggerHandler(Map<Id, Account> oldTriggerAccounts,
                                 Map<Id, Account> newTriggerAccounts) {
                                       	
        oldAccs = oldTriggerAccounts;
        newAccs = newTriggerAccounts;                                   
    }
    
    public void processBeforeInsert(){
    	
    	system.debug('AccountsTriggerHandler:processBeforeInsert:Start');
    	
    	for (Account newAccount : newAccounts) {
    		// put you stuff here!
    		system.debug('AccountsTriggerHandler:processBeforeInsert:BeforeInserttrigger Completed');
    	}
    	system.debug('AccountsTriggerHandler:processBeforeInsert:End');
    }
    
    public void processBeforeUpdate(){
        system.debug('AccountsTriggerHandler:processBeforeUpdate:Start');
        for (Account newAccount : newAccs.values()) {
            
            Account oldAccount = oldAccs.get(newAccount.Id);
            // do you custom stuff here 
            system.debug('AccountsTriggerHandler:processBeforeUpdate:Completed');   
        }
        system.debug('AccountsTriggerHandler:processBeforeUpdate:End');   
    }
    
    public void processBeforeDelete(){
    	
    // do you custom stuff here 
    system.debug('AccountsTriggerHandler:processBeforeDelete:Start');	 		
    system.debug('AccountsTriggerHandler:processBeforeDelete:End');	
    }
    
    public void processAfterUnDelete(){
    	
    // do you custom stuff here 
    system.debug('AccountsTriggerHandler:processAfterUnDelete:Start');	 		
    system.debug('AccountsTriggerHandler:processAfterUnDelete:End');	
    }
    
    public void processAfterInsert(){
    	
    	system.debug('AccountsTriggerHandler:processAfterInsert:Start');
    	
    	for (Account newAccount : newAccounts) {
    		// put you stuff here!
    		system.debug('AccountsTriggerHandler:processBeforeInsert:processAfterInsert Completed');
    	}
    	system.debug('AccountsTriggerHandler:processAfterInsert:End');
    }
    
    public void processAfterUpdate(){
        system.debug('AccountsTriggerHandler:processAfterUpdate:Start');
        for (Account newAccount : newAccs.values()) {
            
            Account oldAccount = oldAccs.get(newAccount.Id);
            // do you custom stuff here 
            system.debug('AccountsTriggerHandler:processAfterUpdate:Completed');   
        }
        system.debug('AccountsTriggerHandler:processAfterUpdate:End');   
    }
    
    public void processAfterDelete(){
    	
    // do you custom stuff here 
    system.debug('AccountsTriggerHandler:processAfterDelete:Start');	 		
    system.debug('AccountsTriggerHandler:processAfterDelete:End');	
    }
}