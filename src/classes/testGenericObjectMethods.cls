/*  
   Created By   :P.W.Tyler   
   Created Date :21/02/2017
   Class Function: unit tests for genericObjectMethods
                   You are free to use this and unit tests or adapt this framework, just keep the author 
                   name and created date in.
                                                             
   Change History: Initial - 21/02/2017

*/
@isTest
public with sharing class testGenericObjectMethods {
	 static testMethod void testCreateUser() {
	 	User user = genericObjectMethods.createUser('System Administrator');
	 	System.assertEquals(user.lastname,'Testing');
	 }
	 
	 static testMethod void testTetAccountDetails() {
	 	Account a = new Account(Name='TestAccount');
	    insert a;
	    
	    //test for an valid account
	    Account acc = genericObjectMethods.getAccountDetails('TestAccount');
	    System.assertEquals(acc.Name,'TestAccount');
	    
	    //test for an invalid account the exception handler
	    Account account = genericObjectMethods.getAccountDetails('XX');
	    
	 }
}