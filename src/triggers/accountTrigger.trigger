/*  
   Created By   :P.W.Tyler   
   Created Date :20/02/2017
   Class Function: Trigger handler on accounts module
                   Just fill in the appropriate trigger by initiaising the created trigger handler class and then 
                   call the required method in the instance.
                   
                   This framework for trigger handling can be used on all salesforce objects and 
                   custom objects by replacing the word Account with the relevant object name, 
                   don't forget __c for custom objects.
                   
                   You are free to use this and unit tests or adapt this framework, just keep the author 
                   name and created date in.
                                              
   Change History: Initial - 20/02/2017

*/

trigger accountTrigger on Account (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
 
system.debug ('accountTrigger:Start');
	if (Trigger.isBefore) {
			
	    if (Trigger.isInsert) {
            system.debug ('accountTrigger:isBefore:isInsert');
            AccountTriggerHandler handler = new AccountTriggerHandler(Trigger.old, Trigger.new);
            handler.processBeforeInsert();
	    } 
        if (Trigger.isUpdate) {	
            system.debug ('accountTrigger:isBefore:isUpdate'); 
            AccountTriggerHandler handler= new AccountTriggerHandler(Trigger.oldMap, Trigger.newMap);
	    	handler.processBeforeUpdate();         
        }
        if (Trigger.isDelete) {
	        system.debug ('accountTrigger:isBefore:isDelete');    
	        AccountTriggerHandler handler = new AccountTriggerHandler();
	        handler.processBeforeDelete();
		} 
    } 

    if (Trigger.IsAfter) {
        if (Trigger.isInsert) { 
            system.debug ('accountTrigger:IsAfter:isInsert');
            AccountTriggerHandler handler = new AccountTriggerHandler(Trigger.old, Trigger.new);
            handler.processAfterInsert();	
        } 
        if (Trigger.isUpdate) {
            system.debug ('accountTrigger:IsAfter:isUpdate');
            AccountTriggerHandler handler= new AccountTriggerHandler(Trigger.oldMap, Trigger.newMap);
	    	handler.processAfterUpdate();    
        }  
        if (Trigger.isDelete) {
	        system.debug ('accountTrigger:IsAfter:isDelete'); 
	        AccountTriggerHandler handler = new AccountTriggerHandler();
	        handler.processAfterDelete(); 
		}  
		if (Trigger.isUndelete) {
	        system.debug ('accountTrigger:IsAfter:isUndelete');  
	        AccountTriggerHandler handler = new AccountTriggerHandler();
	        handler.processAfterUndelete();
   		}
    }   
    system.debug ('accountTrigger:End');
}